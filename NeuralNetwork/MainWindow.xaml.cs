﻿using NeuralNetwork.GUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NeuralNetwork.GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowViewModel mainWindowViewModel;

        public MainWindow()
        {
            InitializeComponent();
            this.mainWindowViewModel = this.FindResource("mainWindowViewModel") as MainWindowViewModel;
        }

        private void ClickLearningButton(object sender, RoutedEventArgs e)
        {
            try
            {
                mainWindowViewModel.Learning();
                PlotWindow w = new PlotWindow(mainWindowViewModel);
                w.Show();
            }
            catch (FormatException ex)
            {
                MessageBox.Show(ex.Message, "Hibás formátum az inputban.");
            }

        }

        private void ClickStatisticButton(object sender, RoutedEventArgs e) => mainWindowViewModel.StatisticStatemenetToCsvFile();
    }
}
