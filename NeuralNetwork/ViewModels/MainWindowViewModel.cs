﻿using NeuralNetwork.GUI.Logic;
using NeuralNetwork.Logic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NeuralNetwork.GUI.ViewModels
{

    public class MainWindowViewModel : Bindable
    {
        #region Local variables

        private string _selectedLearningPattern;
        private string[] _learningPattern;
        private int _numberOfData;
        private int _numberOfHiddenLayers;
        private int[] _numberOfNeurons;
        private int _numberOfEpochs;
        private double _learningRate;
        private string _numberOfNeuronsInput;
        private int _learningState;
        private string _selectedCalculatingMode;
        private string[] _calculatingModes;
        private Visibility textVisitibility;
        private int _numberOfSubNetworks;
        private string _informations;
        private Stopwatch _time;

        private ObservableCollection<KeyValuePair<double, double>> _originalFunction;
        private ObservableCollection<KeyValuePair<double, double>> _learnedFunction;
        private List<KeyValuePair<int, Stopwatch>> stat = new List<KeyValuePair<int, Stopwatch>>();

        #endregion

        #region Properties
        public string[] LearningPatterns { get => _learningPattern; set => _learningPattern = value; }

        public string SelectedLearningPattern { get => _selectedLearningPattern; set => _selectedLearningPattern = value; }

        public int NumberOfData { get => _numberOfData; set => SetProperty(ref _numberOfData, value); }

        public int NumberOfHiddenLayers { get => _numberOfHiddenLayers; set => SetProperty(ref _numberOfHiddenLayers, value); }

        public int NumberOfEpochs { get => _numberOfEpochs; set => SetProperty(ref _numberOfEpochs, value); }

        public double LearningRate { get => _learningRate; set => SetProperty(ref _learningRate, value); }

        public string NumberOfNeuronsInput { get => _numberOfNeuronsInput; set => _numberOfNeuronsInput = value; }        

        public int LearningState { get => _learningState; set => SetProperty(ref _learningState, value); }

        public string SelectedCalculatingMode
        {
            get
            {
                return _selectedCalculatingMode;
            }
            set
            {
                SetProperty(ref _selectedCalculatingMode, value);
                if (_selectedCalculatingMode == _calculatingModes[1])
                {
                    TextVisitibility = Visibility.Visible;
                    NumberOfSubNetworks = 2;
                }
                else
                {
                    TextVisitibility = Visibility.Hidden;
                    NumberOfSubNetworks = 1;
                }
            }
        }

        public string[] CalculatingModes { get => _calculatingModes; set => SetProperty(ref _calculatingModes, value); }

        public Visibility TextVisitibility { get => textVisitibility; set => SetProperty(ref textVisitibility,value); }

        public int NumberOfSubNetworks { get => _numberOfSubNetworks; set =>SetProperty(ref _numberOfSubNetworks,value); }

        public ObservableCollection<KeyValuePair<double, double>> LearnedFunction { get => _learnedFunction; set => SetProperty(ref _learnedFunction, value); }

        public ObservableCollection<KeyValuePair<double, double>> OriginalFunction { get => _originalFunction; set => SetProperty(ref _originalFunction ,value); }
        public Stopwatch Time { get => _time; set => _time = value; }
        public string Informations { get => _informations; set => _informations = value; }

        #endregion

        public MainWindowViewModel()
        {
            this._learningPattern = new string[] { "Sin" };
            this._calculatingModes = new string[] { "Szekvenciális", "Párhuzamos" };
            this._selectedCalculatingMode = _calculatingModes[0];
            this.textVisitibility = Visibility.Hidden;
            this._selectedLearningPattern = _learningPattern[0];
            this._numberOfData = 200;
            this._numberOfHiddenLayers = 1;
            this._numberOfEpochs = 3000;
            this._numberOfNeuronsInput = "10;10";
            this._learningRate = 0.033;
            this._learningState =0;
            this._numberOfSubNetworks = 1;
            _informations = "";
            _time = new Stopwatch();
        }

        private void CheckInputFormat()
        {
            try
            {
                string temp = "1;" + _numberOfNeuronsInput + ";1";
                string[] array = temp.Split(';');

                if (array.Length < 1)
                {
                    throw new FormatException("Nincsen rejtett réteg megadva!\n Rejtett rétegek minimális száma 1!");
                }
                else if (array.Length > 10)
                {
                    throw new FormatException("Túl sok rejtett réteg van megadva!\n Rejtett rétegek maximális száma 10!");
                }

                _numberOfNeurons = new int[array.Length];

                for (int i = 0; i < array.Length; i++)
                {
                    if(Convert.ToInt32(array[i]) < 1)
                    {
                        throw new FormatException("Rejtett rétegben lévő neuronok száma nem lehet 1-nél kisebb!");
                    }
                    else
                    {
                        _numberOfNeurons[i] = Convert.ToInt32(array[i]);
                    }
                }
            }
            catch
            {
                throw new FormatException("Egész számokat írjon be, és azokat ; karakterrel válassza el! Például: 3;5;3");
            }
        }

        public void Learning()
        {
            CheckInputFormat();

            //---------------------------------------------------------------------------------- Init network -----------------------------------------------------------------------------------//
            NetworkControl networkControl = new NetworkControl(_selectedLearningPattern, _selectedCalculatingMode, _numberOfData, _numberOfNeurons, _numberOfEpochs, _learningRate, _numberOfSubNetworks);
            Network neuralNetwork;
            _time.Restart();
            if (_selectedCalculatingMode.Equals(CalculatingModes[0]))                                                               // Sequential training
            {
                _time.Start();
                neuralNetwork = networkControl.TrainingTheNetworkSequence();
                _time.Stop();
            }
            else                                                                                                                                                                    // Parallel training
            {
                _time.Start();
                neuralNetwork = networkControl.TrainingTheNetworkParallelWithTasks();
                _time.Stop();
            }
            _informations = (new StringBuilder("Idő: " + _time.Elapsed.Minutes + " perc " +  _time.Elapsed.Seconds +" másodperc" ).ToString());
            
            //---------------------------------------------------------------------------------- Test network -----------------------------------------------------------------------------------//

            this._learnedFunction = new ObservableCollection<KeyValuePair<double, double>>();
            this._originalFunction = new ObservableCollection<KeyValuePair<double, double>>();
            List<double> X = new List<double>(); 
            List<double> Y = new List<double>();

          

            Parallel.For(0, 60, x =>
            {
                double value = x / 10.0;
                X.Add(value);
                Y.Add(Math.Sin(value));
            });
           

            for (int j = 0; j < X.Count; j++)
            {
                double[] result = neuralNetwork.FeedForward(new double[] { X[j] });

                for (int i = 0; i < result.Length; i++)
                {
                    LearnedFunction.Add(new KeyValuePair<double, double>(X[j], result[i]));
                    OriginalFunction.Add(new KeyValuePair<double, double>(X[j], Y[j]));
                }
            }

        }

        internal void StatisticStatemenetToCsvFile()
        {
            CheckInputFormat();
            stat = new List<KeyValuePair<int, Stopwatch>>();
            // <--------------------------------------------------- Hálózatok futtatása több esetben generálása --------------------------------------------------->
            for (int i = 1; i < 5; i++)
            {
                for (int iteration = 1; iteration < 11; iteration++)
                {
                    _numberOfData = iteration * 100;    // 100 - 1000 -ig
                    _numberOfSubNetworks = i;           // 1-4-ig 
                    if(_numberOfSubNetworks == 1)
                    {
                        _selectedCalculatingMode = CalculatingModes[0]; // Szekvenciális
                    }
                    else
                    {
                        _selectedCalculatingMode = CalculatingModes[1]; // Párhuzamos
                    }
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    LearningForTest();
                    sw.Stop();
                    stat.Add(new KeyValuePair<int, Stopwatch>(_numberOfData, sw));
                }
            }

            StreamWriter fw = new StreamWriter("test.csv", true);
            fw.WriteLine("Number of Data;Time for 1 subnetwork;Time for 2 subnetwork;Time for 3 subnetwork;Time for 4 subnetwork");
            int b = 10;
            for (int i = 0; i < b; i++)
            {
                fw.WriteLine("{0};{1};{2};{3};{4}", stat[i].Key, stat[i].Value.Elapsed.TotalSeconds, stat[i + b].Value.Elapsed.TotalSeconds, stat[i + 2 * b].Value.Elapsed.TotalSeconds, stat[i + 3 * b].Value.Elapsed.TotalSeconds);
            }
            fw.Close();



        }

        private void LearningForTest()
        {
            // -------------------------------------- Init network ---------------------------------------//
            NetworkControl networkControl = new NetworkControl(_selectedLearningPattern, _selectedCalculatingMode, _numberOfData, _numberOfNeurons, _numberOfEpochs, _learningRate, _numberOfSubNetworks);
            Network neuralNetwork;
        
            if (_selectedCalculatingMode.Equals(CalculatingModes[0]))                                                               // Sequential training
            {
                
                neuralNetwork = networkControl.TrainingTheNetworkSequence();
             
            }
            else                                                                                                                                                                    // Parallel training
            {
                
                neuralNetwork = networkControl.TrainingTheNetworkParallelWithTasks();
               
            }
        }
    }
}
