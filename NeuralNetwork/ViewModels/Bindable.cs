﻿namespace NeuralNetwork.GUI.ViewModels
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public class Bindable : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        protected void SetProperty<T>(ref T field, T value, [CallerMemberName] string name = null)
        {
            field = value;
            this.OnPropertyChanged(name);
        }
    }
}
