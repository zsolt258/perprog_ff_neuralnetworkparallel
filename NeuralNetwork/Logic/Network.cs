﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.GUI.Logic
{
    public class Network
    {
        private int[] _numberOfNeurons;
        private int _numberOfEpochs;
        private double _learningRate;
        private int _numberOfData;

        private int[] _numberOfOuputs;
        private int[] _numberOfInputs;
        private double[][] _output;
        private double[][] _input;
        private double[][,] _weights;
        private double[][,] _weightsDelta;
        private double[][] _gamma;
        private double[][] _error;

        private double[] _partOfX;
        private double[] _partOfY;

        private Random _random;

        public double[][,] Weights { get => _weights; set => _weights = value; }

        public Network(int[] numberOfNeurons_, int numberOfEpochs_, double learningRate_, int numberOfData_, double[] partOfX_, double[] partOfY_)
        {
            int size = numberOfNeurons_.Length - 1;

            _numberOfNeurons = new int[numberOfNeurons_.Length];

            for (int i = 0; i < numberOfNeurons_.Length; i++)
            {
                _numberOfNeurons[i] = numberOfNeurons_[i];
            }

            _numberOfEpochs = numberOfEpochs_;
            _learningRate = learningRate_;
            _numberOfData = numberOfData_;
            _numberOfInputs = new int[size];
            _numberOfOuputs = new int[size];
            _output = new double[size][];
            _input = new double[size][];
            _gamma = new double[size][];
            _error = new double[size][];
            _weights = new double[size][,];
            _weightsDelta = new double[size][,];
            _random = new Random();

            for (int i = 0; i < size; i++)
            {
                _numberOfInputs[i] = _numberOfNeurons[i];
                _numberOfOuputs[i] = _numberOfNeurons[i + 1];
                _output[i] = new double[_numberOfOuputs[i]];
                _input[i] = new double[_numberOfInputs[i]];
                _gamma[i] = new double[_numberOfOuputs[i]];
                _error[i] = new double[_numberOfOuputs[i]];
                _weights[i] = new double[_numberOfOuputs[i], _numberOfInputs[i]];
                _weightsDelta[i] = new double[_numberOfOuputs[i], _numberOfInputs[i]];
            }

            _partOfX = new double[numberOfData_];
            _partOfY = new double[numberOfData_];
            for (int i = 0; i < numberOfData_; i++)
            {
                _partOfX[i] = partOfX_[i];
                _partOfY[i] = partOfY_[i];
            }

            for (int i = 0; i < _weights.Length; i++)
            {
                for (int x = 0; x < _weights[i].GetLength(0); x++)
                {
                    for (int y = 0; y < _weights[i].GetLength(1); y++)
                    {
                        _weights[i][x, y] = _random.NextDouble() - 0.5;
                    }
                }
            }

        }

        public void Training()
        {
                for (int j = 0; j < _partOfX.Length; j++)
                {
                    FeedForward(new double[] { _partOfX[j] });
                    BackPropogationError(new double[] { _partOfY[j] });
                }
        }

        public void SequentialTraining()
        {
            for (int i = 0; i < _numberOfEpochs; i++)
            {
                for (int j = 0; j < _partOfX.Length; j++)
                {
                    FeedForward(new double[] { _partOfX[j] });
                    BackPropogationError(new double[] { _partOfY[j] });
                }
            }
        }

        private void BackPropogationError(double[] y_)
        {
            for (int s = _output.Length - 1; s >= 0; s--)
            {
                if (s == _output.Length - 1)
                {

                    for (int i = 0; i < _numberOfOuputs[s]; i++)
                    {
                        _error[s][i] = _output[s][i] - y_[i];
                    }

                    for (int i = 0; i < _numberOfOuputs[s]; i++)
                    {
                        _gamma[s][i] = _error[s][i] * Auxiliary.DerivativeOfTanH(_output[s][i]);
                    }

                    for (int i = 0; i < _numberOfOuputs[s]; i++)
                    {
                        for (int j = 0; j < _numberOfInputs[s]; j++)
                        {
                            _weightsDelta[s][i, j] = _gamma[s][i] * _input[s][j];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < _numberOfOuputs[s]; i++)
                    {
                        _gamma[s][i] = 0;

                        for (int j = 0; j < _gamma[s + 1].Length; j++)
                        {
                            _gamma[s][i] += _gamma[s + 1][j] * _weights[s + 1][j, i];
                        }

                        _gamma[s][i] *= Auxiliary.DerivativeOfTanH(_output[s][i]);
                    }

                    for (int i = 0; i < _numberOfOuputs[s]; i++)
                    {
                        for (int j = 0; j < _numberOfInputs[s]; j++)
                        {
                            _weightsDelta[s][i, j] = _gamma[s][i] * _input[s][j];
                        }
                    }
                }
            }

            UpdateWeights();
        }

        public double[] FeedForward(double[] inputs_)
        {
            double[] output = Auxiliary.MatrixMultiplication(inputs_, _weights[0]);

            for (int i = 0; i < inputs_.Length; i++)
            {
                _input[0][i] = inputs_[i];
            }

            for (int i = 0; i < output.Length; i++)
            {
                _output[0][i] = Math.Tanh(output[i]);
            }

            for (int i = 1; i < _numberOfNeurons.Length - 1; i++)
            {
                for (int j = 0; j < _output[i - 1].Length; j++)
                {
                    _input[i][j] = _output[i - 1][j];
                }

                output = Auxiliary.MatrixMultiplication(_input[i], _weights[i]);
                for (int j = 0; j < output.Length; j++)
                {
                    _output[i][j] = Math.Tanh(output[j]);
                }
            }

            return _output[_output.Length - 1];
        }

        private void UpdateWeights()
        {
            for (int k = 0; k < _numberOfOuputs.Length; k++)
            {
                for (int i = 0; i < _numberOfOuputs[k]; i++)
                {
                    for (int j = 0; j < _numberOfInputs[k]; j++)
                    {
                        _weights[k][i, j] -= _weightsDelta[k][i, j] * _learningRate;
                    }
                }
            }
        }

    }
}
