﻿using NeuralNetwork.GUI.Logic;
using NeuralNetwork.GUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.Logic
{
    public class NetworkControl
    {
        private static Random _random;
        private string _selectedLearningPattern;
        private string _selectedCalculatingMode;

        private int _numberOfData;
        private int _numberOfDataInSubNetwork;
        private int[] _numberOfNeurons;
        private int _numberOfEpochs;
        private double _learningRate;
        private int _numberOfSubNetwork;
        private int _learningState;

        private double[][] X;
        private double[][] Y;


        public NetworkControl(string selectedLearningPattern, string selectedCalculatingMode, int numberOfData, int[] numberOfNeurons, int numberOfEpochs, double learningRate, int numberOfSubNetworks)
        {
            _selectedLearningPattern = selectedLearningPattern;
            _selectedCalculatingMode = selectedCalculatingMode;
            _numberOfData = numberOfData;
            _numberOfNeurons = numberOfNeurons;
            _numberOfEpochs = numberOfEpochs;
            _learningRate = learningRate;
            _numberOfSubNetwork = numberOfSubNetworks;
            _random = new Random();
            _learningState = 0;
            
        }

        public Network TrainingTheNetworkParallelWithTasks()
        {
            //--------------------------------------------------------------------------------- Create training data ---------------------------------------------------------------------------------//

            DivideDataToSubNetworks();
            GenerateTrainingData();

            //--------------------------------------------------------------------------------- Dedicated Network ------------------------------------------------------------------------------------------//

            Network dedicatedNetwork = new Network(_numberOfNeurons, _numberOfEpochs, _learningRate, 1, X[0], Y[0]);

            //--------------------------------------------------------------------------------- Create subnetworks ------------------------------------------------------------------------------------------//

            Network[] networks = new Network[_numberOfSubNetwork];

            for (int i = 0; i < _numberOfSubNetwork; i++)
            {
                int index = i;
                networks[index] = new Network(_numberOfNeurons, _numberOfEpochs, _learningRate, _numberOfDataInSubNetwork, X[index], Y[index]);

                for (int j = 0; j < dedicatedNetwork.Weights.Length; j++)
                {
                    int j_index = j;
                    for (int x = 0; x < dedicatedNetwork.Weights[j].GetLength(0); x++)
                    {
                        int x_index = x;
                        for (int y = 0; y < dedicatedNetwork.Weights[j].GetLength(1); y++)
                        {
                            int y_index = y;
                            networks[index].Weights[j_index][x_index, y_index] = dedicatedNetwork.Weights[j_index][x_index, y_index];
                        }
                    }
                }
            }

            //------------------------------------------------------------------------------- Start ---------------------------------------------------------------------------------------------//

            double[][,] NewWeights = new double[_numberOfNeurons.Length - 1][,];

            double state = _numberOfEpochs / 100;

            for (int epoch = 0; epoch < _numberOfEpochs+1; epoch++)
            {

                Parallel.For(0, _numberOfSubNetwork, sub =>
                {
                    networks[sub].Training();
                });


                for (int j = 0; j < _numberOfNeurons.Length - 1; j++)
                {
                    NewWeights[j] = new double[_numberOfNeurons[j + 1], _numberOfNeurons[j]];
                }

                // Súlyok összeadása
                for (int j = 0; j < _numberOfSubNetwork; j++)
                {
                    for (int i = 0; i < networks[j].Weights.Length; i++)
                    {
                        for (int x = 0; x < networks[j].Weights[i].GetLength(0); x++)
                        {
                            for (int y = 0; y < networks[j].Weights[i].GetLength(1); y++)
                            {
                                NewWeights[i][x, y] += networks[j].Weights[i][x, y];
                            }
                        }
                    }
                }

                Parallel.For(0, NewWeights.Length, i =>
                {
                    for (int m = 0; m < NewWeights[i].GetLength(0); m++)
                    {
                        for (int n = 0; n < NewWeights[i].GetLength(1); n++)
                        {
                            NewWeights[i][m,n] /= _numberOfSubNetwork;
                        }
                    }
                });

                for (int i = 0; i < _numberOfSubNetwork; i++)
                {
                    for (int j = 0; j < networks[i].Weights.Length; j++)
                    {
                        for (int x = 0; x < networks[i].Weights[j].GetLength(0); x++)
                        {
                            for (int y = 0; y < networks[i].Weights[j].GetLength(1); y++)
                            {
                                networks[i].Weights[j][x, y] = NewWeights[j][x, y];
                            }
                        }
                    }
                }
            }



            for (int j = 0; j < dedicatedNetwork.Weights.Length; j++)
            {
                for (int x = 0; x < dedicatedNetwork.Weights[j].GetLength(0); x++)
                {
                    for (int y = 0; y < dedicatedNetwork.Weights[j].GetLength(1); y++)
                    {
                        dedicatedNetwork.Weights[j][x, y] = NewWeights[j][x, y];
                    }
                }
            }
            return dedicatedNetwork;
        }

        public Network TrainingTheNetworkSequence()
        {
            int temp = _numberOfSubNetwork;
            _numberOfDataInSubNetwork = _numberOfData;
            GenerateTrainingData();
            Network net = new Network(_numberOfNeurons, _numberOfEpochs, _learningRate, _numberOfDataInSubNetwork, X[0], Y[0]);
            net.SequentialTraining();
            _numberOfDataInSubNetwork = temp;
            return net;
        }

        private void GenerateTrainingData()
        {
            X = new double[_numberOfSubNetwork][];
            Y = new double[_numberOfSubNetwork][];

            for (int i = 0; i < _numberOfSubNetwork; i++)
            {
                X[i] = new double[_numberOfDataInSubNetwork];
                Y[i] = new double[_numberOfDataInSubNetwork];

                if (_selectedLearningPattern.Equals("Sin"))
                {
                    for (int j = 0; j < _numberOfDataInSubNetwork; j++)
                    {
                        X[i][j] = _random.NextDouble() * 3 * Math.PI;
                        Y[i][j] = Math.Sin(X[i][j]);
                    }
                }
                else
                {
                    for (int j = 0; j < _numberOfDataInSubNetwork; j++)
                    {
                        X[i][j] = _random.NextDouble() * 100;
                        Y[i][j] = Math.Cos(X[i][j]);
                    }
                }
               
            }

        }

        private void DivideDataToSubNetworks()
        {
            int mod = _numberOfData % _numberOfSubNetwork;
            _numberOfData += mod;
            _numberOfDataInSubNetwork = _numberOfData / _numberOfSubNetwork;
        }

    }
}
