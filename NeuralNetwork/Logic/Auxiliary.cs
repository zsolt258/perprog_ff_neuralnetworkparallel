﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.GUI.Logic
{
    static class Auxiliary
    {
        public static double[] MatrixMultiplication(double[] v1_, double[,] v2_)
        {
            double[] resultMatrix = new double[v2_.GetLength(0)];
            for (int m = 0; m < v2_.GetLength(0); m++)
            {
                for (int n = 0; n < v2_.GetLength(1); n++)
                {
                    if (v1_.Length == 1)
                    {
                        resultMatrix[m] += v2_[m, n] * v1_[0];
                    }
                    else
                    {
                        resultMatrix[m] += v2_[m, n] * v1_[n];
                    }
                }
            }
            //Parallel.For(0, v2_.GetLength(0), m =>
            //{
            //    for (int n = 0; n < v2_.GetLength(1); n++)
            //    {
            //        if (v1_.Length == 1)
            //        {
            //            resultMatrix[m] += v2_[m, n] * v1_[0];
            //        }
            //        else
            //        {
            //            resultMatrix[m] += v2_[m, n] * v1_[n];
            //        }
            //    }
            //});


            return resultMatrix;
        }

        public static double DerivativeOfTanH(double value)
        {
            return 1 - (value * value);
        }

    }
}
